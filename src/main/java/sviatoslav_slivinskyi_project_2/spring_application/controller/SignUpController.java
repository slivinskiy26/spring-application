package sviatoslav_slivinskyi_project_2.spring_application.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sviatoslav_slivinskyi_project_2.spring_application.dto.UserDTO;
import sviatoslav_slivinskyi_project_2.spring_application.model.User;
import sviatoslav_slivinskyi_project_2.spring_application.service.UserService;


@RestController
@RequestMapping("/signup")
public class SignUpController {

    @Autowired
    private UserService userService;

    private static final Logger LOGGER = LogManager.getLogger(SignUpController.class);


    @PostMapping
    public ResponseEntity<UserDTO> signUpUser(@RequestBody UserDTO userDTO) {
        if (userService.getUserByName(userDTO.getUsername()) != null) {
            LOGGER.error("username already exists");
            return ResponseEntity.badRequest().build();
        }
        if (userDTO.getPassword().length() < 7) {
            LOGGER.error("password too small");
            return ResponseEntity.badRequest().build();
        }
        UserDTO savedUser = convertUserToUserDTO(userService.insertUser(convertUserDTOToUser(userDTO)));
        LOGGER.info("User created successfully");
        return ResponseEntity.ok(savedUser);
    }


    private UserDTO convertUserToUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        return userDTO;
    }

    private User convertUserDTOToUser(UserDTO userDTO) {
        User user = new User();
        BeanUtils.copyProperties(userDTO, user);
        return user;
    }
}
