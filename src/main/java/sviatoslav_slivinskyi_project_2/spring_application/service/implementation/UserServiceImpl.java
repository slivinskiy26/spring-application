package sviatoslav_slivinskyi_project_2.spring_application.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sviatoslav_slivinskyi_project_2.spring_application.model.User;
import sviatoslav_slivinskyi_project_2.spring_application.repository.UserRepository;
import java.security.SecureRandom;
import java.util.Base64;
import sviatoslav_slivinskyi_project_2.spring_application.service.HashService;
import sviatoslav_slivinskyi_project_2.spring_application.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private HashService hashService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;



    @Override
    public User insertUser(User user){
        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassWord(encodedPassword);
        return userRepository.save(user);
    }

    @Override
    public User getUserByName(String username){
        return userRepository.getUserByUsername(username);
    }
}
