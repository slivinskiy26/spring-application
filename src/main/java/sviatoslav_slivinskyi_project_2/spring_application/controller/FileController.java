package sviatoslav_slivinskyi_project_2.spring_application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sviatoslav_slivinskyi_project_2.spring_application.dto.FileDTO;
import sviatoslav_slivinskyi_project_2.spring_application.model.File;
import sviatoslav_slivinskyi_project_2.spring_application.service.FileService;
import sviatoslav_slivinskyi_project_2.spring_application.service.UserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/files")
public class FileController {

    @Autowired
    private FileService fileService;
    @Autowired
    private UserService userService;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileController.class);

    private static final int MaxUploadFileSize = 105242880;

    @GetMapping("/{username}")
    public List<FileDTO> viewFiles(@PathVariable String username){
        List<FileDTO> files = convertFileListToFileDTO(fileService.findAllFiles(username));
        return files;
    }

    @DeleteMapping("/deleteFile/{fileId}")
    public void deleteFile(@PathVariable Long fileId){
        fileService.deleteFile(fileId);
        if (fileService.getOptionalFile(fileId).isPresent()) {
            LOGGER.error("An error occurs during deleting the file");
        } else {
            LOGGER.info("The file was deleted");
        }
    }

    @GetMapping("/downloadFile/{fileId}")
    public ResponseEntity <byte[]> downloadFile(@PathVariable Long fileId){
        FileDTO fileDTO = convertFileToFileDTO(fileService.getFile(fileId));
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ORIGIN, fileDTO.getFileName());
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(fileDTO.getContentType()))
                .headers(headers)
                .body(fileDTO.getFileData());
    }

    @PostMapping("/uploadFile/{username}")
    public ResponseEntity<FileDTO> uploadFile(@RequestParam MultipartFile file, @PathVariable String username) throws IOException {
        if (file.getSize() > MaxUploadFileSize){
            LOGGER.error("File too long");
            return (ResponseEntity<FileDTO>) ResponseEntity.badRequest();
        } else {
            FileDTO fileDTO = new FileDTO();
            fileDTO.setUser(userService.getUserByName(username));
            fileDTO.setFileData(file.getBytes());
            fileDTO.setFileName(file.getOriginalFilename());
            fileDTO.setFileSize(String.valueOf(file.getSize()));
            fileDTO.setContentType(file.getContentType());
            FileDTO savedFile = convertFileToFileDTO(fileService.uploadFile(convertFileDTOToFile(fileDTO)));
            if (savedFile == null) {
                LOGGER.error("An error occurs during uploading the note");
            } else {
                LOGGER.info("The file was uploaded");
            }
            return ResponseEntity.ok(savedFile);
        }
    }

    private FileDTO convertFileToFileDTO(File file){
        FileDTO fileDTO = new FileDTO();
        BeanUtils.copyProperties(file, fileDTO);
        return fileDTO;
    }

    private File convertFileDTOToFile(FileDTO fileDTO){
        File file = new File();
        BeanUtils.copyProperties(fileDTO, file);
        return file;
    }

    private List<FileDTO> convertFileListToFileDTO(List<File> files){
        List<FileDTO> fileDTOS = new ArrayList<>();
        files.forEach(file -> fileDTOS.add(convertFileToFileDTO(file)));
        return fileDTOS;
    }
}
