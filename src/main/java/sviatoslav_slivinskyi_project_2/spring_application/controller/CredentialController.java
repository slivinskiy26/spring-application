package sviatoslav_slivinskyi_project_2.spring_application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sviatoslav_slivinskyi_project_2.spring_application.dto.CredentialDTO;
import sviatoslav_slivinskyi_project_2.spring_application.model.Credential;
import sviatoslav_slivinskyi_project_2.spring_application.service.CredentialService;
import sviatoslav_slivinskyi_project_2.spring_application.service.EncryptionService;
import sviatoslav_slivinskyi_project_2.spring_application.service.UserService;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/credentials")
public class CredentialController {

    @Autowired
    private CredentialService credentialService;
    @Autowired
    private EncryptionService encryptionService;
    @Autowired
    private UserService userService;

    private static final int MIN_SQL_RESULT = 0;

    private static final Logger LOGGER = LoggerFactory.getLogger(CredentialController.class);

    @GetMapping("/username/{username}")
    public List<CredentialDTO> viewCredentials(@PathVariable String username){
        return convertCredentialListToCredentialDTO(credentialService.getAllUserCredentials(username));
    }

    @GetMapping("/{credentialsId}")
    public ResponseEntity<CredentialDTO> getCredentialsById(@PathVariable Long credentialsId){
        CredentialDTO credentialDTO = convertCredentialToCredentialDTO(credentialService.getCredentialsById(credentialsId));
        String decryptedPassword = encryptionService.decryptValue(credentialDTO.getPassword(), credentialDTO.getSecretKey());
        credentialDTO.setPassword(decryptedPassword);
        return ResponseEntity.ok(credentialDTO);
    }

    @PostMapping("/newCredentials/{username}")
    public ResponseEntity<CredentialDTO> addCredentials(@RequestBody CredentialDTO credentialDTO, @PathVariable String username){
        credentialDTO.setUser(userService.getUserByName(username));
        CredentialDTO savedCredential = convertCredentialToCredentialDTO(credentialService.saveCredentials(convertCredentialDTOToCredential(credentialDTO)));

        if(savedCredential.getCredentialId() > MIN_SQL_RESULT){
            LOGGER.info("Credentials were saved");
        } else {
            LOGGER.error("An error occurs during deleting credentials");
        }
        return ResponseEntity.ok(savedCredential);
    }


    @DeleteMapping("/deleteCredentials/{credentialId}")
    public void deleteCredentials(@PathVariable Long credentialId){
        credentialService.deleteCredentials(credentialId);
        if (credentialService.getOptionalCredentials(credentialId).isPresent()){
            LOGGER.error("An error occurs during deleting credentials");
        } else {
            LOGGER.info("Credentials were deleted");
        }
    }

    @PutMapping("/updateCredentials/{credentialId}")
    public void updateCredentials(@PathVariable Long credentialId, @RequestBody CredentialDTO credentialDTO){
        CredentialDTO credentialDTO1 = convertCredentialToCredentialDTO(credentialService.getCredentialsById(credentialDTO.getCredentialId()));
        String encryptedPassword = encryptionService.encryptValue(credentialDTO.getPassword(), credentialDTO1.getSecretKey());
        credentialDTO.setPassword(encryptedPassword);
        int result = credentialService.updateCredentials(convertCredentialDTOToCredential(credentialDTO));
        if (result < MIN_SQL_RESULT){
            LOGGER.error("An error occurs during updating credentials");
        } else {
            LOGGER.info("Credentials were updated");
        }
    }


    private CredentialDTO convertCredentialToCredentialDTO(Credential credential){
        CredentialDTO credentialDTO = new CredentialDTO();
        BeanUtils.copyProperties(credential, credentialDTO);
        return credentialDTO;
    }

    private Credential convertCredentialDTOToCredential(CredentialDTO credentialDTO){
        Credential credential = new Credential();
        BeanUtils.copyProperties(credentialDTO, credential);
        return credential;
    }

    private List<CredentialDTO> convertCredentialListToCredentialDTO(List<Credential> credentials){
        List<CredentialDTO> credentialDTOS = new ArrayList<>();
        credentials.forEach(credential -> credentialDTOS.add(convertCredentialToCredentialDTO(credential)));
        return credentialDTOS;
    }

}
