package sviatoslav_slivinskyi_project_2.spring_application.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sviatoslav_slivinskyi_project_2.spring_application.dto.NoteDTO;
import sviatoslav_slivinskyi_project_2.spring_application.model.Note;
import sviatoslav_slivinskyi_project_2.spring_application.service.NoteService;
import sviatoslav_slivinskyi_project_2.spring_application.service.UserService;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/notes")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @Autowired
    private UserService userService;

    private static final int MIN_SQL_RESULT = 0;

    private static final Logger LOGGER = LogManager.getLogger(NoteController.class);

    @GetMapping("/username/{username}")
    public List<NoteDTO> viewNotes(@PathVariable String username) {
        return convertNoteListToNoteDTO(noteService.getAllNotes(username));
    }

    @GetMapping("/{noteId}")
    public ResponseEntity<NoteDTO> getNoteById(@PathVariable Long noteId) {
        NoteDTO noteDTO = convertNoteToNoteDTO(noteService.getNote(noteId));
        return ResponseEntity.ok(noteDTO);
    }

    @PostMapping("/newNote/{username}")
    public ResponseEntity<NoteDTO> addNote(@RequestBody NoteDTO noteDTO, @PathVariable String username) {
        noteDTO.setUser(userService.getUserByName(username));
        NoteDTO savedNote = convertNoteToNoteDTO(noteService.saveNote(convertNoteDTOToNote(noteDTO)));

        if (savedNote.getNoteId() > MIN_SQL_RESULT) {
            LOGGER.info("Note was saved");
        } else {
            LOGGER.error("An error occurs during saving the note");
        }
        return ResponseEntity.ok(savedNote);
    }

    @DeleteMapping("/deleteNote/{noteId}")
    public void deleteNote(@PathVariable Long noteId) {
        noteService.deleteNote(noteId);
        if (noteService.getOptionalNote(noteId).isPresent()) {
            LOGGER.error("Note wasn`t deleted");
        } else {
            LOGGER.info("Note successfully deleted");
        }
    }

    @PutMapping("/updateNote/{noteId}")
    public void updateNote(@PathVariable Long noteId, @RequestBody NoteDTO noteDTO) {
        noteDTO.setNoteId(noteId);
        int result = noteService.updateNote(convertNoteDTOToNote(noteDTO));
        if (result < MIN_SQL_RESULT) {
            LOGGER.error("An error occurs during updating the note");
        } else {
            LOGGER.info("Note was updated");
        }
    }

    private NoteDTO convertNoteToNoteDTO(Note note) {
        NoteDTO noteDTO = new NoteDTO();
        BeanUtils.copyProperties(note, noteDTO);
        return noteDTO;
    }

    private Note convertNoteDTOToNote(NoteDTO noteDTO) {
        Note note = new Note();
        BeanUtils.copyProperties(noteDTO, note);
        return note;
    }

    private List<NoteDTO> convertNoteListToNoteDTO(List<Note> notes) {
        List<NoteDTO> noteDTOs = new ArrayList<>();
        notes.forEach(note -> noteDTOs.add(convertNoteToNoteDTO(note)));
        return noteDTOs;
    }

}
